import os

bind = '0.0.0.0:8080'
backlog = 2048

workers = int(os.environ.get('GUNICORN_PROCESSES', '3'))
threads = int(os.environ.get('GUNICORN_THREADS', '1'))


# Askbot in Openshift


## Create common configmap

    $ cat .env
    MYSQL_DATABASE=askbot
    MYSQL_PASSWORD=password
    MYSQL_ROOT_PASSWORD=password
    MYSQL_USER=askbot
    MYSQL_HOST=askbot-db
    $ oc delete configmap/askbot || :
    $ oc create configmap askbot --from-env-file=.env

## Deploy database

    $ export LABELS='-l app=askbot,service=db'
    $ oc new-app ${LABELS} \\
        --name=asbot-db \
        --env-file=.env \
        -e MYSQL_DATADIR_FIRST_INIT=1
        registry.access.redhat.com/rhscl/mariadb-101-rhel7
    $ oc set env dc/askbot-db MYSQL_HOST=
    $ oc logs -f dc/askbot-db

## Deploy application

    $ export LABELS='-l app=askbot,service=app'
    $ oc new-app --name askbot-app $LABELS https://gitlab.cee.redhat.com/openshift/askbot.git~centos/python-27-centos7
    $ oc create route edge askbot-app --service askbot-app --hostname=askbot.cloud.paas.lab.upshift.redhat.com

    $ oc set env bc/askbot-app GIT_SSL_NO_VERIFY=1
    $ oc set env dc/askbot-app --from configmap/askbot
    $ oc start-build bc/askbot-app

    $ oc logs -f dc/askbot-app
